# Code Quality

There are some key aspects that qualify code as good code, 
we will discuss these now. 

## Maintainability 

### Readability

Writing a whole Application is nothing that can be done
quickly. Depending on the Application it can take weeks, months or even 
years. During that time it's quite common to swap 
back and forth and *refactor* certain code blocks as the 
requirements to your application are agile and are bound to change.
Even if you are done releasing your first version, you'll likely get 
to the point you'll have to revisit certain parts. Or maybe you're 
working on a team - all members should be able to understand you and your code
to maybe help you out on certain issues or help you improve. Another example would be
you are working on a project and you get shifted to other projects, so someone else
has to take over your work but they need to understand your work as well. So having a high 
**readability** is vital for good maintainability.

On my latest full stack application, I was having about 550,000 lines of code. Imagine you have to 
find a certain aspect that is supposed to be changed if everything is within 1 file and
or just a wall of text. Developers typically agree on 
some code conventions per project and language - most commonly used 
are the [Google style guides](https://google.github.io/styleguide/).

#### Formatting 

Aside from sticking to your code conventions you should always make use of 
**Comments and documentation** and **blank lines** to structure your code. 
There is no golden rule to determine how to use these aside from having 2 blank lines after each
`import`and function definition in Python. I am usually using a ratio like:

- 55% source code, so executable code 
- 30% comments and documentation
- 15% blank lines. 

**TODO: EXAMPLES**


### Code duplication

Code duplication is another important point. You should never have duplicate or redundant code. If you 
have to update the specific part you'll have to update the duplicate as well which you might forget. 
If you have the same or almost the same functionality across multiple areas of your application you should be using a 
function to do so. 

**todo: examples, see boolean e.g.**

testing something 

